//Priprava knjižnic
var formidable = require("formidable");
var util = require('util');

if (!process.env.PORT)
  process.env.PORT = 8080;

// Priprava povezave na podatkovno bazo
var sqlite3 = require('sqlite3').verbose();
var pb = new sqlite3.Database('chinook.sl3');

// Priprava strežnika
var express = require('express');
var expressSession = require('express-session');
var streznik = express();
streznik.set('view engine', 'ejs');
streznik.use(express.static('public'));
streznik.use(
  expressSession({
    secret: '1234567890QWERTY', // Skrivni ključ za podpisovanje piškotkov
    saveUninitialized: true,    // Novo sejo shranimo
    resave: false,              // Ne zahtevamo ponovnega shranjevanja
    cookie: {
      maxAge: 3600000           // Seja poteče po 60min neaktivnosti
    }
  })
);

var razmerje_usd_eur = 0.877039116;

function davcnaStopnja(izvajalec, zanr) {
  switch (izvajalec) {
    case "Queen": case "Led Zepplin": case "Kiss":
      return 0;
    case "Justin Bieber":
      return 22;
    default:
      break;
  }
  switch (zanr) {
    case "Metal": case "Heavy Metal": case "Easy Listening":
      return 0;
    default:
      return 9.5;
  }
}

// Prikaz seznama pesmi na strani
streznik.get('/', function(zahteva, odgovor) {
  
  //Dodajanje preverjanja seje
  //Ce seja obstaja izvede kot ponavadi
  
  var trenutnaSeja = zahteva.session.stranka; //definicija trenutne seje za kasnejso uporabo
  
  if (trenutnaSeja) {                          

  pb.all("SELECT Track.TrackId AS id, Track.Name AS pesem, \
          Artist.Name AS izvajalec, Track.UnitPrice * " +
          razmerje_usd_eur + " AS cena, \
          COUNT(InvoiceLine.InvoiceId) AS steviloProdaj, \
          Genre.Name AS zanr \
          FROM Track, Album, Artist, InvoiceLine, Genre \
          WHERE Track.AlbumId = Album.AlbumId AND \
          Artist.ArtistId = Album.ArtistId AND \
          InvoiceLine.TrackId = Track.TrackId AND \
          Track.GenreId = Genre.GenreId \
          GROUP BY Track.TrackId \
          ORDER BY steviloProdaj DESC, pesem ASC \
          LIMIT 100", function(napaka, vrstice) {
    if (napaka)
      odgovor.sendStatus(500);
    else {
        for (var i=0; i<vrstice.length; i++)
          vrstice[i].stopnja = davcnaStopnja(vrstice[i].izvajalec, vrstice[i].zanr);
        odgovor.render('seznam', {seznamPesmi: vrstice});
      }
  })
  }
  else {
    odgovor.redirect('/prijava');    //Ce seja ne obstaja, redirecta uporabnika na  /prijava 

  }
})

// Dodajanje oz. brisanje pesmi iz košarice
streznik.get('/kosarica/:idPesmi', function(zahteva, odgovor) {
  var idPesmi = parseInt(zahteva.params.idPesmi);
  if (!zahteva.session.kosarica)
    zahteva.session.kosarica = [];
  if (zahteva.session.kosarica.indexOf(idPesmi) > -1) {
    zahteva.session.kosarica.splice(zahteva.session.kosarica.indexOf(idPesmi), 1);
  } else {
    zahteva.session.kosarica.push(idPesmi);
  }
  
  odgovor.send(zahteva.session.kosarica);
});

// Vrni podrobnosti pesmi v košarici iz podatkovne baze
var pesmiIzKosarice = function(zahteva, callback) {
  if (!zahteva.session.kosarica || Object.keys(zahteva.session.kosarica).length == 0) {
    callback([]);
  } else {
    pb.all("SELECT Track.TrackId AS stevilkaArtikla, 1 AS kolicina, \
    Track.Name || ' (' || Artist.Name || ')' AS opisArtikla, \
    Track.UnitPrice * " + razmerje_usd_eur + " AS cena, 0 AS popust, \
    Genre.Name AS zanr \
    FROM Track, Album, Artist, Genre \
    WHERE Track.AlbumId = Album.AlbumId AND \
    Artist.ArtistId = Album.ArtistId AND \
    Track.GenreId = Genre.GenreId AND \
    Track.TrackId IN (" + zahteva.session.kosarica.join(",") + ")",
    function(napaka, vrstice) {
      if (napaka) {
        callback(false);
      } else {
        for (var i=0; i<vrstice.length; i++) {
          vrstice[i].stopnja = davcnaStopnja((vrstice[i].opisArtikla.split(' (')[1]).split(')')[0], vrstice[i].zanr);
        }
        callback(vrstice);
      }
    })
  }
}

streznik.get('/kosarica', function(zahteva, odgovor) {
  pesmiIzKosarice(zahteva, function(pesmi) {
    if (!pesmi)
      odgovor.sendStatus(500);
    else
      odgovor.send(pesmi);
  });
})

// Vrni podrobnosti pesmi na računu
var pesmiIzRacuna = function(racunId, callback) {
    pb.all("SELECT Track.TrackId AS stevilkaArtikla, 1 AS kolicina, \
    Track.Name || ' (' || Artist.Name || ')' AS opisArtikla, \
    Track.UnitPrice * " + razmerje_usd_eur + " AS cena, 0 AS popust, \
    Genre.Name AS zanr \
    FROM Track, Album, Artist, Genre \
    WHERE Track.AlbumId = Album.AlbumId AND \
    Artist.ArtistId = Album.ArtistId AND \
    Track.GenreId = Genre.GenreId AND \
    Track.TrackId IN (SELECT InvoiceLine.TrackId FROM InvoiceLine, Invoice \
    WHERE InvoiceLine.InvoiceId = Invoice.InvoiceId AND Invoice.InvoiceId = " + racunId + ")",
    function(napaka, vrstice) {
      callback(vrstice); //callback <- spremenjeno iz console.log(vrstice) zato da dobimo podatke

    })
}

// Vrni podrobnosti o stranki iz računa
var strankaIzRacuna = function(racunId, callback) {
    pb.all("SELECT Customer.* FROM Customer, Invoice \
            WHERE Customer.CustomerId = Invoice.CustomerId AND Invoice.InvoiceId = " + racunId,
    function(napaka, vrstice) {

      callback(vrstice); //callback <- spremenjeno iz console.log(vrstice) zato da dobimo podatke

    })
}

// Izpis računa v HTML predstavitvi na podlagi podatkov iz baze
streznik.post('/izpisiRacunBaza', function(zahteva, odgovor) {
  //odgovor.end(); namesto tega 
  //PODOBNO KOT izpis racuna v HTML oz XML obliki
  //Prikaze racun, ki je shranjen v bazi
  //preveri ali obstajajo pesmi in podatki stranke
  //postavke racuna in stranka v e-slogu nastavi na sledece,
  //e-slog dobi definicijo
  var form = new formidable.IncomingForm();
  
  form.parse(zahteva, function(napaka, polja, datoteke){
    var seznamRacunov = polja.seznamRacunov;                //definiramo seznam racunov za kasnejso uporabo
    
    //dobimo podatke iz funkcij zato ker stranka ni samo ena
    strankaIzRacuna(seznamRacunov, function(podatkiStranke){
      pesmiIzRacuna(seznamRacunov, function(pesmi){
          if (!pesmi || !podatkiStranke) {          //preverimo ali obsataja
            odgovor.sendStatus(500);
          } else if (pesmi.length == 0) {           //ce pesmi ni
            odgovor.send("<p>V košarici nimate nobene pesmi, \
              zato računa ni mogoče pripraviti!</p>");
          } else {                                  //drugace definiraj v eslog

            odgovor.setHeader('content-type', 'text/xml');
            odgovor.render('eslog', {
              vizualiziraj: true,
              postavkeRacuna: pesmi,
              stranka: podatkiStranke[0]
          })  
        }
      });
    });
  });
});

var stranka = function(strankaId, callback) {
  pb.get("SELECT Customer.* FROM Customer WHERE Customer.CustomerId = " + strankaId, //$cid", {$cid: strankaId},
    function(napaka, vrstica) {
      callback(vrstica);
  });
}

// Izpis računa v HTML predstavitvi ali izvorni XML obliki
streznik.get('/izpisiRacun/:oblika', function(zahteva, odgovor) {
  //dopolnitev funkcije
  pesmiIzKosarice(zahteva, function(pesmi) {
    var sejaStranke = zahteva.session.stranka; //definiramo sejo stranke za kasnejso uporabo
    if (!pesmi) {
      odgovor.sendStatus(500);
    } else if (pesmi.length == 0) {
      odgovor.send("<p>V košarici nimate nobene pesmi, \
        zato računa ni mogoče pripraviti!</p>");
    } else {
      //klicemo stranko da izvemo informacije za zapis v racun
      stranka (sejaStranke, function(podatkiStranka){
      odgovor.setHeader('content-type', 'text/xml');
      odgovor.render('eslog', {
        vizualiziraj: zahteva.params.oblika == 'html' ? true : false,

        postavkeRacuna: pesmi,
        stranka: podatkiStranka
      }) 
    })

    }
  })
  
})

// Privzeto izpiši račun v HTML obliki
streznik.get('/izpisiRacun', function(zahteva, odgovor) {
  odgovor.redirect('/izpisiRacun/html')
})

// Vrni stranke iz podatkovne baze
var vrniStranke = function(callback) {
  pb.all("SELECT * FROM Customer",
    function(napaka, vrstice) {
      callback(napaka, vrstice);
    }
  );
}

// Vrni račune iz podatkovne baze
var vrniRacune = function(callback) {
  pb.all("SELECT Customer.FirstName || ' ' || Customer.LastName || ' (' || Invoice.InvoiceId || ') - ' || date(Invoice.InvoiceDate) AS Naziv, \
          Invoice.InvoiceId \
          FROM Customer, Invoice \
          WHERE Customer.CustomerId = Invoice.CustomerId",
    function(napaka, vrstice) {
      callback(napaka, vrstice);
    }
  );
}

// Registracija novega uporabnika
streznik.post('/prijava', function(zahteva, odgovor) {
  var form = new formidable.IncomingForm();
  
  form.parse(zahteva, function (napaka, polja, datoteke) {

    //variables defined, for easier later use;
    var ime = polja.FirstName;    var priimek = polja.LastName;
    var podjetje = polja.Company; var naslov = polja.Address;
    var mesto = polja.City;       var state = polja.State;
    var drzava = polja.Country;   var postnaSt = polja.PostalCode;
    var telefon = polja.Phone;    var mail = polja.Email;
    var fax = polja.Fax;
    
    //preveriti ce je kaksno polje prazno (polje.)
    if(ime==0 || priimek == 0 || podjetje == 0 || naslov == 0 || mesto == 0 ||
       state == 0|| drzava == 0 || postnaSt == 0 || telefon == 0 || mail == 0 ||fax == 0 ){

         vrniStranke(function(napaka1, stranke) {
                  vrniRacune(function(napaka2, racuni) {
                    odgovor.render('prijava', {sporocilo: "Prišlo je do napake pri registraciji nove stranke. Prosim preverite vnešene podatke in poskusite znova.",
                                    seznamStrank: stranke, seznamRacunov: racuni});  
                  }) 
                }); 
    }
    else {

      //dodaj stranko v bazo 
      pb.run("INSERT INTO Customer (FirstName, LastName, Company, Address, City, State, Country, PostalCode, \
	            Phone, Fax, Email, SupportRepId) VALUES ($fn,$ln,$com,$addr,$city,$state,$country,$pc,$phone,$fax,$email,$sri)", 
	            {$fn: ime , $ln: priimek , $com: podjetje ,$addr: naslov ,$city: mesto ,$state: state,  
	                 $country: drzava,$pc:postnaSt ,$phone : telefon ,$fax: fax ,$email: mail ,$sri:3}, 

	             function(napaka) {
	              vrniStranke(function(napaka1, stranke) {
                  vrniRacune(function(napaka2, racuni) {
                    odgovor.render('prijava', {sporocilo: "Stranka je bila uspešno registrirana.", seznamStrank: stranke, seznamRacunov: racuni});  
                  }) 
                });
      });
      
    }
    
  }
  

  
)})

// Prikaz strani za prijavo
streznik.get('/prijava', function(zahteva, odgovor) {
  vrniStranke(function(napaka1, stranke) {
      vrniRacune(function(napaka2, racuni) {
        odgovor.render('prijava', {sporocilo: "", seznamStrank: stranke, seznamRacunov: racuni});  
      }) 
    });
})

// Prikaz nakupovalne košarice za stranko
streznik.post('/stranka', function(zahteva, odgovor) {
  var form = new formidable.IncomingForm();

  //nepravilno delovanje
  //if (!zahteva.session.stranka ) {
    //console.log ("kek");
  //  form.parse(zahteva, function (napaka1, polja, datoteke) {
   // odgovor.redirect('/prijava');
   // });
  //}
  //else {

  form.parse(zahteva, function (napaka1, polja, datoteke) {
    zahteva.session.stranka = parseInt(polja.seznamStrank);  //nastavi sejo stranke
    odgovor.redirect('/')
   // console.log ("kk");
  });

 // }

})

// Odjava stranke
streznik.post('/odjava', function(zahteva, odgovor) {

   //Ponastavitev košarice

    zahteva.session.kosarica = null;
    zahteva.session.stranka = null;
    odgovor.redirect('/prijava') 
})



streznik.listen(process.env.PORT, function() {
  console.log("Strežnik pognan!");
})
